import type { AWS } from "@serverless/typescript";

const serverlessConfiguration: AWS = {
  service: "template-serverless",
  frameworkVersion: "2",
  custom: {
    ["serverless-offline"]: {
      httpPort: 3333,
    },
    webpack: {
      webpackConfig: "./webpack.config.js",
      includeModules: {
        packagePath: "./package.json",
        forceInclude: ["mysql"],
      },
    },
    prune: {
      automatic: true,
      number: 3,
    },
    dotenv: {
      required: {
        file: true,
      },
      exclude: ["AWS_ACCESS-KEY-ID", "AWS_SECRET_ACCESS_KEY"],
    },
    stagesDomain: {
      stg: "stg-",
      dev: "dev-",
    },
    customDomain: {
      domainName:
        "${self:custom.stagesDomains.${self:provider.stage},''}internal-api.example.com",
      basePath: "${self:provider.stage}",
      stage: "${self:provider.stage}",
      // certificateArn:"",
      createRoute53Record: true,
    },
  },

  plugins: [
    "serverless-webpack",
    "serverless-offline",
    "serverless-dotenv-plugin",
    "serverless-prune-plugin",
    "serverless-domain-manager",
  ],
  provider: {
    name: "aws",
    runtime: "nodejs12.x",
    stage: "${opt:stage}",
    // role:"",
    tracing: {
      apiGateway: true,
      lambda: true,
    },
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
    },
    lambdaHashingVersion: "20201221",
    // vpc: { securityGroupIds: [""], subnetIds: [] },
  },
  functions: {
    hello: {
      handler: "src/functions/index.helloFunction",
      events: [
        {
          http: {
            method: "post",
            path: "hello/{id}",
            // request: {
            //   parameters: { querystrings: { ["id"]: true } },
            // },
          },
        },
      ],
    },
  },
};

module.exports = serverlessConfiguration;
