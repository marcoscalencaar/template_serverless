process.env.TZ = "GMT";

module.exports = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    "<rootDir>/**/*.ts",
    "!**/migrations/**.ts",
    "!**/handlerStatus/**.ts",
    "!**/error/**.ts",
  ],
  coverageDirectory: "coverage",
  coverageProvider: "babel",
  coverageReporters: ["text-summary", "lcov"],
  preset: "ts-jest",
  testEnvironment: "node",
  testMatch: ["**/*.(test|spec).test"],
  testPatchIgnorePatterns: [],
};
