import {
  Connection,
  getConnection,
  ConnectionManager,
  ConnectionOptions,
  createConnections,
  getConnectionManager,
} from "typeorm";

export class TypeOrmDatabase {
  private connectionManager: ConnectionManager;

  constructor() {
    this.connectionManager = getConnectionManager();
  }

  public async getConnection(connectionName: string): Promise<Connection> {
    let conn: Connection;

    if (this.connectionManager.has(connectionName)) {
      console.log(`Database.getConnection()-using existing connection ...`);
      conn = this.connectionManager.get(connectionName);

      if (!conn.isConnected) {
        conn = await conn.connect();
      }
    } else {
      console.log(`Database.getConnection()-creating connection ...`);

      const connectionOptions: ConnectionOptions[] = [
        {
          name: `example`,
          type: `mysql`,
          port: 5432,
          synchronize: false,
          logging: false,
          host: process.env.DB_HOST,
          username: process.env.DB_USERNAME,
          database: "example-db",
          password: process.env.DB_PASSWORD,
          entities: [],
          maxQueryExecutionTime: 30000,
        },
      ];

      await createConnections(connectionOptions);
      conn = getConnection(connectionName);
    }

    return conn;
  }
}
