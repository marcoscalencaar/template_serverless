export default {
  type: "object",
  properties: {
    name: { type: "string" },
  },
  required: ["name"],
} as const;

export interface IEvent {
  body: {
    name: string;
  };
  headers: {};
  pathParameters: { id: number };
}
