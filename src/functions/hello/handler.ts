import "source-map-support/register";
import { Context } from "aws-lambda/handler";
import { formatJSONResponse } from "@utils/apiGateway";
import type { ValidatedEventAPIGatewayProxyEvent } from "@utils/apiGateway/interfaces";

import schema from "./interfaces";
import { IEvent } from "./interfaces";
import { HelloService } from "@services/hello/index";

export const hello: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (
  event: IEvent,
  context: Context
) => {
  try {
    const helloService: HelloService = new HelloService();
    const res = await helloService.get(event);
    return formatJSONResponse(res, 200);
  } catch (error) {
    return formatJSONResponse(error, error.httpCode || 500);
  }
};
