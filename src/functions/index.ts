import { middyfy } from "@utils/bodyParser";

import { hello } from "./hello/handler";

export const helloFunction = middyfy(hello);
