import { APIError } from "@utils/errors/index";
import { IEvent } from "./interfaces";

export class HelloService {
  private name: string;

  private setName(name: string) {
    this.name = name;
  }
  public async get(event: IEvent) {
    try {
      this.setName(event.body.name);
      // throw new Error("test");

      var arr = [];
      while (arr.length < 121) {
        var randValue =
          "<br>" +
          Math.round(Math.random() * 10) +
          "+" +
          Math.round(Math.random() * 10) +
          "=";
        arr.push();
        // Check duplicate elements before push
        if (arr.indexOf(randValue) == -1) arr.push(randValue);
      }
      return {
        message: `Hello ${arr}, welcome to the exciting Serverless world!`,
        event,
      };
    } catch (error) {
      throw new APIError("BadRequest", 500, true, error.message);
    }
  }
}
