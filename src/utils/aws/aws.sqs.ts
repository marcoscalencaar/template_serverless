import { SQS } from "aws-sdk";

const sqs = new SQS({ apiVersion: "2012-11-05" });

export async function sendMessage(params: SQS.SendMessageRequest) {
  return sqs.sendMessage(params).promise();
}

export async function deleteMessage(params: SQS.Types.DeleteMessageRequest) {
  return sqs.deleteMessage(params).promise();
}
export async function getQueueAttributes(
  params: SQS.Types.GetQueueAttributesRequest
) {
  return sqs.getQueueAttributes(params).promise();
}
export async function sendMessageBatch(
  params: SQS.Types.SendMessageBatchRequest
) {
  return sqs.sendMessageBatch(params).promise();
}
