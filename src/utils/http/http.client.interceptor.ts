import axios, { AxiosInstance, AxiosResponse } from "axios";
/*
  Código de referencia para futuras consultas
  https://levelup.gitconnected.com/enhance-your-http-request-with-axios-and-typescript-f52a6c6c2c8e 
*/

declare module "axios" {
  interface AxiosResponse<T = any> extends Promise<T> {}
}

export abstract class HttpClient {
  protected readonly instance: AxiosInstance;

  public constructor(baseURL: string, timeout: number) {
    this.instance = axios.create({
      baseURL,
      timeout,
    });

    this._initializeResponseInterceptor();
  }

  private _initializeResponseInterceptor = () => {
    this.instance.interceptors.response.use(
      this._handlerResponse,
      this._handlerError
    );
  };

  private _handlerResponse = async (data: AxiosResponse) => {
    return data.data;
  };
  protected _handlerError = (error: any) => Promise.reject(error);
}
