import { StatusCode } from "status-code-enum";

export class BaseError extends Error {
  public readonly name: string;
  public readonly httpCode: StatusCode;
  public readonly isOperational: boolean;
  public readonly description: string;

  constructor(
    name: string,
    httpCode: StatusCode,
    description: string,
    isOperational: boolean
  ) {
    super(description);
    Object.setPrototypeOf(this, new.target.prototype);

    this.name = name;
    this.httpCode = httpCode;
    this.isOperational = isOperational;
    this.description = description;

    Error.captureStackTrace(this);
  }
}

export class APIError extends BaseError {
  constructor(
    name: string,
    httpCode = StatusCode.ServerErrorInternal,
    isOperational = true,
    description = "internal server error"
  ) {
    super(name, httpCode, description, isOperational);
  }
}
